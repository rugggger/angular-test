import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

export interface IProduct {
    id: number;
    name: string;
    description: string;
    price: number;
    creationDate: Date;
    thumbnailUrl: string;
    url: string;
}

export interface IProductEvent {
    id: number;
}

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
    @Input() product: IProduct;
    @Output('deleteProduct') deleteProduct = new EventEmitter();
    @Output('displayProduct') displayProduct = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onDelete($event) {
    const event: IProductEvent = {
        id: this.product.id
    };
    this.deleteProduct.emit(event);
    $event.stopPropagation();

  }
  onClick() {
      const event: IProductEvent = {
          id: this.product.id
      };
      this.displayProduct.emit(event);
  }

}
