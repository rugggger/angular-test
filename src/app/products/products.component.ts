import { Component, OnInit } from '@angular/core';
import {ProductsService} from '../services/ProductsService/products.service';
import {IProduct, IProductEvent} from './product/product.component';
import {IMessage, MessageService} from '../services/MessageService/message.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products;
  private sortBy: string;
  private filter: string;
  private subscription: Subscription;

  constructor(private service: ProductsService, private messageService: MessageService) {
      this.subscription = this.messageService.getMessage()
          .subscribe((message: IMessage) => {
            switch (message.type) {
                case 'sort':
                    this.sortBy = message.data;
                break;
                case 'filter':
                    this.filter = message.data;
                    break;
                case 'add':
                    this.addProduct();
                break;
                case 'save':
                    this.saveProduct(message.data);
                break;
            }

          });
  }

  ngOnInit() {
      this.sortBy = 'name';
      this.service.getProducts()
          .subscribe(response => {
              this.products = response;
          });
  }

  addProduct() {
      const newProduct: IProduct = {
          id: Math.floor(Math.random() * 1000000),
          name: 'New Product',
          description: '',
          price: 0,
          creationDate: new Date(Date.now()),
          url: '',
          thumbnailUrl: ''
      };

      this.products.push(newProduct);

  }
  saveProduct(product: IProduct) {
      const id = product.id;
      const index = this.products.findIndex((el => el.id === id));
      this.products[index] = product;
  }
  onDeleteProduct($event: IProductEvent) {
      const id = $event.id;
      const index = this.products.findIndex((product => product.id === id));
      this.products.splice(index, 1);
  }

  onDisplayProduct($event: IProductEvent) {
      const id = $event.id;
      const product = this.products.find(el => el.id === id);
      this.messageService.displayProduct(product);
  }

}
