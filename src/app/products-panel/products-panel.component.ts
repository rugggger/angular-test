import { Component, OnInit } from '@angular/core';
import { MessageService} from '../services/MessageService/message.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';



@Component({
  selector: 'app-products-panel',
  templateUrl: './products-panel.component.html',
  styleUrls: ['./products-panel.component.scss']
})
export class ProductsPanelComponent implements OnInit {

  private productsPanel;
  private select;
  private formModel;
  constructor(
      public fb: FormBuilder,
      private messageService: MessageService ) { }
  ngOnInit() {
        this.formModel = this.fb.group({
            'search': [''],
            'select': ['name']

        });
       // this.select = new FormControl('');
    }
 addProduct() {
     this.messageService.addProduct();

 }
 changeFilter() {
   this.messageService.changeFilter(this.formModel.get('search').value);
 }
  changeSort() {
    this.messageService.changeSort(this.formModel.get('select').value);
  }
}
