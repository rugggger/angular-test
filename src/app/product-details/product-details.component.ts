import { Component, OnInit } from '@angular/core';
import {IProduct} from '../products/product/product.component';
import {MatDialog} from '@angular/material';
import {SaveProductDialogComponent} from '../save-product-dialog/save-product-dialog.component';
import {FormBuilder, FormGroup , Validators} from '@angular/forms';
import {IMessage, MessageService} from '../services/MessageService/message.service';
import {Subscription} from 'rxjs';
import {PriceValidators} from '../common/price.validators';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  private product: IProduct;
  private formModel: FormGroup;
  private subscription: Subscription;
  private image: string;

  constructor(
      public dialog: MatDialog,
      public fb: FormBuilder,
      private messageService: MessageService
  ) {
      this.subscription = this.messageService.getMessage()
          .subscribe((message: IMessage) => {
              if (message.type === 'display') {
                  const product = message.data as IProduct;
                  this.product = product;
                  this.image = product.url;
                  this.formModel.patchValue({
                      'name': product.name,
                      'description': product.description,
                      'price': product.price
                  });
              }
          });
  }

  ngOnInit() {
    this.formModel = this.fb.group({
        'name': ['', Validators.required],
        'description': [''],
        'price': ['', [Validators.required, PriceValidators.aboveZero]]
    });

  }
  saveProduct() {
      this.product.name = this.formModel.get('name').value;
      this.product.description = this.formModel.get('description').value;
      this.product.price = this.formModel.get('price').value;
      this.messageService.saveProduct(this.product);
      this.openDialog();
  }

  openDialog(): void {
        const dialogRef = this.dialog.open(SaveProductDialogComponent, {
            data: {
                productName: this.formModel.value.name
            }

        });


  }

}
