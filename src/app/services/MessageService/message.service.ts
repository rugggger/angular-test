import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {IProduct} from '../../products/product/product.component';

export interface IMessage {
    type: string;
    data?: any;
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private subject = new Subject<any>();
  displayProduct(product: IProduct) {
    this.subject.next({
        type: 'display',
        data: product
    });
  }
  changeFilter(filter: string) {
        this.subject.next({
            type: 'filter',
            data: filter
        });
  }
  changeSort(sort: string) {
    this.subject.next({
        type: 'sort',
        data: sort
    });
  }
  addProduct() {
        this.subject.next({
            type: 'add'
        });
  }
  saveProduct(product: IProduct) {
      this.subject.next({
          type: 'save',
          data: product
      });
  }
  clearMessage() {
    this.subject.next();
  }
  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

}
