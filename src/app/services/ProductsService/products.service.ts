import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
    readonly url = 'https://msbit-exam-products-store.firebaseio.com/products.json';

  constructor(private http: HttpClient) {

  }

  getProducts() {
      return this.http.get(this.url);
  }

}
