import { Pipe, PipeTransform } from '@angular/core';
import {IProduct} from '../../products/product/product.component';

@Pipe({
  name: 'filterProducts'
})
export class FilterProductsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (args === null || args === undefined || args === '') {return value; }
    if (!Array.isArray(value)) {return value; }

    const searchString = (args as String).toLowerCase();
    return (value as Array<IProduct>).filter((el: IProduct) => el.name.toLowerCase().indexOf(searchString) > -1 );

  }

}
