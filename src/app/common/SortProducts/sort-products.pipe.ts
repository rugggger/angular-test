import { Pipe, PipeTransform } from '@angular/core';
import {IProduct} from '../../products/product/product.component';

@Pipe({
  name: 'sortProducts',
  pure: false

})
export class SortProductsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      if (!Array.isArray(value)) {return value; }

      let newArr;
      if (args === 'name') {
         newArr = (value as Array<IProduct>).sort(
              (product1, product2) => {
                  if (product1.name < product2.name) { return -1; }
                  if (product1.name > product2.name) { return 1; }
                  return 0;

              } );
        return newArr;
      }
      if (args === 'recent') {
           newArr = (value as Array<IProduct>).sort(
              (product1, product2) => {
                  if (product1.creationDate < product2.creationDate) { return 1; }
                  if (product1.creationDate > product2.creationDate) { return -1; }
                  return 0;

              } );
          return newArr;
      }
    return value;
  }

}
