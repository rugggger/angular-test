import {AbstractControl, ValidationErrors} from '@angular/forms';


export class PriceValidators {
    static aboveZero(control: AbstractControl): ValidationErrors | null {
        if ((control.value as number) <= 0) {
            return { aboveZero: 'Price needs to be above zero!' };
        }
    }
}