import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import {ProductsService} from './services/ProductsService/products.service';
import { ProductComponent } from './products/product/product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material';
import { SaveProductDialogComponent } from './save-product-dialog/save-product-dialog.component';
import { ProductsPanelComponent } from './products-panel/products-panel.component';
import { SortProductsPipe } from './common/SortProducts/sort-products.pipe';
import { FilterProductsPipe } from './common/FilterProducts/filter-products.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ProductComponent,
    ProductDetailsComponent,
    SaveProductDialogComponent,
    ProductsPanelComponent,
    SortProductsPipe,
    FilterProductsPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  entryComponents: [
    SaveProductDialogComponent
  ],
  providers: [
      ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
